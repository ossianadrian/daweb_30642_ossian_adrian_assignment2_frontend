import "bootstrap/dist/css/bootstrap.min.css";
import React, { Component } from 'react';
import ReactNotification from "react-notifications-component";
import axios from "axios";

class Appointments extends Component {


    constructor(props) {
        super(props);

        this.appointment = this.appointment.bind(this);
        this.handleChangeInputEmail = this.handleChangeInputEmail.bind(this);
        this.handleChangeInputDate = this.handleChangeInputDate.bind(this);
        this.state = {
            email: "",
            date:  "",
            doctor:  ""
        };

    }

    appointment() {

        let e = document.getElementById("exampleFormControlSelect1");

        // console.log(e);
        let theDoctor = e.value;


        const object = {
            patient_email: this.state.email,
            doctor: theDoctor,
            date_appointment: this.state.date
        };

        axios.post('http://localhost:8000/appointments' , object )
            .then((res) => {
                console.log(res.data)
                console.log('Successfully created appointment')
            }).catch((error) => {
            console.log(error)
        })
    }

    handleChangeInputEmail(event) {
        this.setState({email: event.target.value});
    }

    handleChangeInputDate(event) {
        this.setState({date: event.target.value});
    }

    componentDidMount() {
        this.setState( {email: localStorage.getItem('email')});
    }

    render() {
        return (

                <div className="Appointments"
                    style={{
                        position: "absolute",
                        top: "50%",
                        left: "50%",
                        transform: "translate(-50%, -50%)"
                    }}
                >
                <form>
                    <div className="form-group">
                        <label htmlFor="exampleFormControlInput1">Email address</label>
                        <input type="email" className="form-control" id="exampleFormControlInput1"
                               placeholder="email@email.com" value={this.state.email} onChange={this.handleChangeInputEmail}/>
                        <label htmlFor="exampleFormControlInput2">Date</label>
                        <input type="date" className="form-control" id="exampleFormControlInput2"
                               placeholder="22.06.2020" value={this.state.date} onChange={this.handleChangeInputDate}/>


                    </div>

                    <div className="form-group">
                        <label htmlFor="exampleFormControlSelect1">Doctor</label>
                        <select className="form-control" id="exampleFormControlSelect1">
                            <option>Medic1</option>
                            <option>Medic2</option>
                            <option>Medic3</option>
                            <option>Medic4</option>
                            <option>Medic5</option>
                        </select>
                    </div>
                    <button type="button" className="btn btn-primary btn-lg" onClick={this.appointment}>Submit</button>

                </form>


            </div>

        );
    }
}
export default Appointments;
