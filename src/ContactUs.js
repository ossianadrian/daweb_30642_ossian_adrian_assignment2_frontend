import "bootstrap/dist/css/bootstrap.min.css";
// import "./App.css"
import React, { Component } from 'react';
import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'
import { store } from 'react-notifications-component';
import {withTranslation} from "react-i18next";



class ContactUs extends Component {

    constructor(props) {
        super(props);
        this.sendEmail = this.sendEmail.bind(this);
        this.handleChangeInputSender = this.handleChangeInputSender.bind(this);
        this.handleChangeInputAddress = this.handleChangeInputAddress.bind(this);
        this.handleChangeInputBody = this.handleChangeInputBody.bind(this);
        this.handleChangeInputPhone = this.handleChangeInputPhone.bind(this);
        this.state = {
            sender: "",
            phone:  "",
            address:  "",
            body: ""
        };

    }

    sendEmail(){


        fetch('http://127.0.0.1:5000/contact/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                sender: this.state.sender,
                phone: this.state.phone,
                address: this.state.address,
                body: this.state.body
            })
        }).then((response) => response.status)
            .then((status) => {
                if(status === 200){
                    console.log("Email send!");
                    store.addNotification({
                        title: "Email send!",
                        message: "We will get back to you with more details as soon as possible.",
                        type: "success",
                        insert: "top",
                        container: "top-right",
                        animationIn: ["animate__animated", "animate__fadeIn"],
                        animationOut: ["animate__animated", "animate__fadeOut"],
                        dismiss: {
                            duration: 5000,
                            onScreen: true
                        }
                    });
                }
                return status;
            })
            .catch((error) => {
                store.addNotification({
                    title: "Email not send!",
                    message: "Please review your input and try again later.",
                    type: "danger",
                    insert: "top",
                    container: "top-right",
                    animationIn: ["animate__animated", "animate__fadeIn"],
                    animationOut: ["animate__animated", "animate__fadeOut"],
                    dismiss: {
                        duration: 5000,
                        onScreen: true
                    }
                });
                console.error(error);
            });
    }

    handleChangeInputSender(event) {
        this.setState({sender: event.target.value});
    }

    handleChangeInputPhone(event) {
        this.setState({phone: event.target.value});
    }

    handleChangeInputAddress(event) {
        this.setState({address: event.target.value});
    }

    handleChangeInputBody(event) {
        this.setState({body: event.target.value});
    }


    componentDidMount() {

    }


    render() {

        const { t } = this.props;
        return (

            <div className="ContactUs">

                <div className="mb-3">
                    <label htmlFor="exampleFormControlInput0" className="form-label">{t('Form.Field1')}</label>
                    <input type="email" className="form-control" id="exampleFormControlInput0"
                           placeholder="name@example.com" value={this.state.sender} onChange={this.handleChangeInputSender}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="exampleFormControlInput1" className="form-label">{t('Form.Field2')}</label>
                    <input type="email" className="form-control" id="exampleFormControlInput1"
                           placeholder="+40748417531" value={this.state.phone} onChange={this.handleChangeInputPhone}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="exampleFormControlInput3" className="form-label">{t('Form.Field3')}</label>
                    <input type="email" className="form-control" id="exampleFormControlInput3"
                           placeholder="Str. Frunzisului 29/7, Cluj-Napoca" value={this.state.address} onChange={this.handleChangeInputAddress}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="exampleFormControlTextarea1" className="form-label">{t('Form.Field4')}</label>
                    <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" value={this.state.body} onChange={this.handleChangeInputBody}></textarea>
                </div>
                <button type="button" className="btn btn-primary btn-lg" onClick={this.sendEmail}>{t('Form.Button')}</button>
                <ReactNotification />


            </div>

        );
    }
}

export default withTranslation()(ContactUs);
