import "bootstrap/dist/css/bootstrap.min.css";
import "./Doctors.css"
import {Component} from "react";
import {withTranslation} from "react-i18next";

class Doctors extends Component {
    render() {
        const { t } = this.props;
        return (

            <div className="Doctors">

                <div className="card-group">

                    <div className="card">
                        <img src="img/doctor1.jpg" className="card-img-top" alt="..."/>
                        <div className="card-body">
                            <h5 className="card-title">Ionut Anghel</h5>
                            <p className="card-text">{t('Doctors.Doctor1.Function')}</p>
                        </div>
                    </div>
                    <div className="card">
                        <img src="img/doctor5.jpg" className="card-img-top" alt="..."/>
                        <div className="card-body">
                            <h5 className="card-title">Steven Wolfe</h5>
                            <p className="card-text">{t('Doctors.Doctor2.Function')}</p>
                        </div>
                    </div>
                    <div className="card">
                        <img src="img/doctor3.jfif" className="card-img-top" alt="..."/>
                        <div className="card-body">
                            <h5 className="card-title">Jim Black</h5>
                            <p className="card-text">{t('Doctors.Doctor3.Function')}</p>
                        </div>
                    </div>
                    <div className="card">
                        <img src="img/doctor4.jfif" className="card-img-top" alt="..."/>
                        <div className="card-body">
                            <h5 className="card-title">Aboha Aluhaha</h5>
                            <p className="card-text">{t('Doctors.Doctor4.Function')}</p>
                        </div>
                    </div>

                </div>


            </div>

        );
    }
}

export default withTranslation()(Doctors);