import "bootstrap/dist/css/bootstrap.min.css";
import React, { Component } from 'react';
import axios from 'axios';

class PersonHome extends Component {


    constructor(props) {
        super(props);
        this.handleChangeInputEmail = this.handleChangeInputEmail.bind(this);
        this.handleChangeInputPassword = this.handleChangeInputPassword.bind(this);
        this.save = this.save.bind(this);
        this.state = {
            email: "",
            password:  "",
            services:  "",
            gdpr: ""
        };

    }

    componentDidMount() {
        this.setState( {email: localStorage.getItem('email')});
        this.setState( {password: localStorage.getItem('password')});
        this.setState( {services: localStorage.getItem('services')});
        this.setState( {gdpr: localStorage.getItem('gdpr')});



    }

    handleChangeInputEmail(event) {
        this.setState({email: event.target.value});
    }

    handleChangeInputPassword(event) {
        this.setState({password: event.target.value});
    }



    save(){
            let e = document.getElementById("ddlViewBy");

        // console.log(e);
        let servicess = e.value;
        let path = document.getElementById("formFile");
        console.log(path.value);
        let theEmail = this.state.email;

        const object = {
            id: 1,
            user_email: theEmail,
            services: servicess,
            gdpr: true
        };

        axios.put('http://localhost:8000/patient/' + this.state.email, object )
            .then((res) => {
                console.log(res.data)
                console.log('Successfully updated')
            }).catch((error) => {
            console.log(error)
        })

    }

    render() {
        return (

            <div className="PersonHome">

                <form>
                    <div className="form-group">
                        <label htmlFor="exampleFormControlInput1" >Email address</label>
                        <input type="email" className="form-control" id="exampleFormControlInput1"
                               placeholder="name@example.com" value={this.state.email} onChange={this.handleChangeInputEmail}/>
                        {/*<label htmlFor="exampleFormControlInput2">Name</label>*/}
                        {/*<input  className="form-control" id="exampleFormControlInput2"*/}
                        {/*       placeholder="Georgel George"/>*/}
                        <label htmlFor="exampleFormControlInput3">Password</label>
                        <input  type="password" className="form-control" id="exampleFormControlInput3"
                                placeholder="Password" onChange={this.handleChangeInputPassword}/>

                            <label htmlFor="formFile" className="form-label">GDPR</label>
                            <input className="form-control" type="file" id="formFile"/>
                    </div>

                    <div className="form-group">
                        <label htmlFor="exampleFormControlSelect2">Example multiple select</label>
                        <select id="ddlViewBy" multiple className="form-control" >
                            <option>Extract tooth 1</option>
                            <option>Extract tooth 2</option>
                            <option>Extract tooth 3</option>
                            <option>Extract tooth 4</option>
                            <option>Extract tooth 1</option>
                        </select>
                    </div>
                    <button type="button" className="btn btn-primary btn-lg" onClick={this.save}>Submit changes</button>
                </form>


            </div>

        );
    }
}
export default PersonHome;
