import "bootstrap/dist/css/bootstrap.min.css";
import {withTranslation} from "react-i18next";
import {Component} from "react";
// import "./App.css"

class ServicesAndPricing extends Component {
    render() {
        const {t} = this.props;
        return (

            <div className="ServicesAndPricing">


                <table className="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">{t('Services.Type')}</th>
                        <th scope="col">{t('Services.Pricing')}</th>
                        <th scope="col">{t('Services.Duration')}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>{t('Services.Service1')}</td>
                        <td>50 lei</td>
                        <td>30 min</td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>{t('Services.Service2')}</td>
                        <td>100 lei</td>
                        <td>20 min</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>{t('Services.Service3')}</td>
                        <td>10000 lei</td>
                        <td>2 h</td>
                    </tr>
                    <tr>
                        <th scope="row">4</th>
                        <td>{t('Services.Service4')}</td>
                        <td>10000 lei</td>
                        <td>2 h</td>
                    </tr>
                    <tr>
                        <th scope="row">5</th>
                        <td>{t('Services.Service5')}</td>
                        <td>70 lei</td>
                        <td>10 min</td>
                    </tr>
                    <tr>
                        <th scope="row">6</th>
                        <td>{t('Services.Service6')}</td>
                        <td>100 lei</td>
                        <td>2 h</td>
                    </tr>
                    <tr>
                        <th scope="row">7</th>
                        <td>{t('Services.Service7')}</td>
                        <td>500 lei</td>
                        <td>2 h</td>
                    </tr>

                    </tbody>
                </table>

            </div>

        );
    }
}

export default withTranslation()(ServicesAndPricing);