import "bootstrap/dist/css/bootstrap.min.css";
import "./Doctors.css"
import React, { Component } from 'react';

class News extends Component {


    constructor(props) {
        super(props);

    }

    componentDidMount() {


        fetch('xml/news.xml')
            .then(function(resp){
                return resp.text();
            })
            .then(function(data){
                let parser = new DOMParser(),
                    xmlDoc = parser.parseFromString(data, 'text/xml');
               let allNews = xmlDoc.getElementsByTagName('news');
               var i = 0;

               var cardGroup = document.getElementById("card-group");

               for(i; i < allNews.length; i++){
                   var img_path = allNews[i].getElementsByTagName('img_path')[0].textContent;
                   var card_title = allNews[i].getElementsByTagName('card_title')[0].textContent;
                   var card_text = allNews[i].getElementsByTagName('card_text')[0].textContent;
                   var href = allNews[i].getElementsByTagName('href')[0].textContent;

                   var cardElement = document.createElement("div");
                   cardElement.setAttribute('className',"card");
                   var imgEl = document.createElement("img");
                   imgEl.setAttribute('src', img_path);
                   imgEl.setAttribute('className', "card-img-top");
                   imgEl.setAttribute('alt', "...");

                   var cardBody = document.createElement("div");
                   cardElement.setAttribute('className',"card-body");

                   var h5 = document.createElement("h5");
                   h5.setAttribute('className',"card-title");
                   h5.textContent = card_title;

                   var p = document.createElement("p");
                   p.setAttribute('className',"card-text");
                   p.textContent = card_text;

                   var a = document.createElement("a");
                   a.setAttribute('href',href);
                   a.setAttribute('className',"btn btn-primary");
                   a.textContent = "Read more";

                   cardBody.appendChild(h5);
                   cardBody.appendChild(p);
                   cardBody.appendChild(a);


                   cardElement.appendChild(imgEl);
                   cardElement.appendChild(cardBody);
                   // cardGroup.appendChild(cardElement);


               }
            });


    }

    render() {
        return (

            <div className="News">


                <div className="card-group" id="card-group">

                    <div className="card">
                        <img src="img/4.jfif" className="card-img-top" alt="..."/>
                        <div className="card-body">
                            <h5 className="card-title">For the seniors</h5>
                            <p className="card-text">We are taking care of you faster.</p>
                            <a href="https://ro.wikipedia.org/wiki/Doctor" className="btn btn-primary">Read more</a>
                        </div>
                    </div>
                    <div className="card">
                        <img src="img/2.jpg" className="card-img-top" alt="..."/>
                        <div className="card-body">
                            <h5 className="card-title">Visit us</h5>
                            <p className="card-text">Research shows you are not visiting your doctor enough.</p>
                            <a href="https://ro.wikipedia.org/wiki/Doctor" className="btn btn-primary">Read more</a>
                        </div>
                    </div>
                    <div className="card">
                        <img src="img/3.jfif" className="card-img-top" alt="..."/>
                        <div className="card-body">
                            <h5 className="card-title">New equipment</h5>
                            <p className="card-text">Latest gen equipment for our pacients.</p>
                            <a href="https://ro.wikipedia.org/wiki/Doctor" className="btn btn-primary">Read more</a>
                        </div>
                    </div>
                    <div className="card">
                        <img src="img/5.jfif" className="card-img-top" alt="..."/>
                        <div className="card-body">
                            <h5 className="card-title">Best team</h5>
                            <p className="card-text">We are hiring.</p>
                            <a href="https://ro.wikipedia.org/wiki/Doctor" className="btn btn-primary">Read more</a>
                        </div>
                    </div>

                </div>

                <div className="card-group">

                    <div className="card">
                        <img src="img/6.jfif" className="card-img-top" alt="..."/>
                        <div className="card-body">
                            <h5 className="card-title">New research</h5>
                            <p className="card-text">Experts say you are not visiting the doctor enough.</p>
                            <a href="https://ro.wikipedia.org/wiki/Doctor" className="btn btn-primary">Read more</a>
                        </div>
                    </div>
                    <div className="card">
                        <img src="img/7.jfif" className="card-img-top" alt="..."/>
                        <div className="card-body">
                            <h5 className="card-title">Programming</h5>
                            <p className="card-text">In search for the best apps.</p>
                            <a href="https://ro.wikipedia.org/wiki/Doctor" className="btn btn-primary">Read more</a>
                        </div>
                    </div>

                    <div className="card">
                        <img src="img/9.jfif" className="card-img-top" alt="..."/>
                        <div className="card-body">
                            <h5 className="card-title">You might suffer of ED</h5>
                            <p className="card-text">No boner? You should book an appointment.</p>
                            <a href="https://ro.wikipedia.org/wiki/Doctor" className="btn btn-primary">Read more</a>
                        </div>
                    </div>

                    <div className="card">
                        <img src="img/8.jfif" className="card-img-top" alt="..."/>
                        <div className="card-body">
                            <h5 className="card-title">Best team award</h5>
                            <p className="card-text">We are the best.</p>
                            <a href="https://ro.wikipedia.org/wiki/Doctor" className="btn btn-primary">Read more</a>
                        </div>
                    </div>

                </div>

            </div>

        );
    }
}
export default News;
