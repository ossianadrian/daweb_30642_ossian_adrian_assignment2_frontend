import "bootstrap/dist/css/bootstrap.min.css";
import {withTranslation} from "react-i18next";
import {Component} from "react";
// import "./App.css"

class Home extends Component {
    render() {
        const { t } = this.props;
        return (


            <div className="Home">
                <h1>{t('Home.WelcomeMessage')}</h1>


                <iframe title="Promo video" width="560" height="315" src="https://www.youtube.com/embed/NM_B8iQUqgw"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowFullScreen></iframe>


            </div>

        );
    }
}
export default withTranslation()(Home);
