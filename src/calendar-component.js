import React, { Component, useState, useEffect } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const Calendar = ({ selected, onSelectedChange }) => {
    const [date, setDate] = useState(selected);

    useEffect(() => {
        setDate(selected);
    }, [selected]);

    return (
        <DatePicker selected={date} onChange={(date) => onSelectedChange(date)} />
    );
};

export default Calendar;
