import React, { Component, useState, useEffect } from "react";
import ListGroup from "react-bootstrap/ListGroup";

export const ListComponent = ({ selectedDate, items }) => {
    const [listItems, setListItems] = useState([]);

    useEffect(() => {
        setListItems([]);
        const theItem = items.filter((item) => item.date === selectedDate);
        setListItems(theItem);
    }, [selectedDate]);

    return (
        <ListGroup>
            {listItems &&
            listItems.map((listItem, idx) => (
                <ListGroup.Item key={idx.toString()}>{listItem.item}</ListGroup.Item>
            ))}
        </ListGroup>
    );
};
export default ListComponent;
