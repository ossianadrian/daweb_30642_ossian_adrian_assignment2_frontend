import {
    Row,
    Container,
    Col,
    Carousel,
    ListGroup,
    Card,
    Button
} from "react-bootstrap";
import React, { Component, useState, useEffect } from "react";
// import "./styles.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Calendar from "./calendar-component.js";
import ListComponent from "./list-component";
import axios from "axios";
import {forEach} from "react-bootstrap/ElementChildren";


export default function DoctorHome() {
    const [selectedDate, setSelectedDate, items] = useState(new Date());
    const [appointmentsList] = useState([]);
    const selectedDateChange = (date) => {
        setSelectedDate(date);
    };
    useEffect(() => {
        getAllAppointments();
    }, []);

    const getAllAppointments = () => {
        axios.get('http://localhost:8000/appointments' )
            .then((res) => {

                for(let item of res.data){
                    let dict = {
                        date: new Date(item.date_appointment).toLocaleDateString("en-US"),
                        item: item.patient_email
                    };
                    appointmentsList.push(dict);
                }
                console.log('Successfully fetched appointments')
            }).catch((error) => {
            console.log(error)
        })

    }

    return (
        <div>
            <h1>Here are all the appointments for you, Doctor!</h1>
            <label>{selectedDate.toDateString()}</label>
            <Calendar selected={selectedDate} onSelectedChange={selectedDateChange} />
            <ListComponent
                    selectedDate={selectedDate.toLocaleDateString("en-US")}
                    // items= {[
                    //     { date: "4/13/2021", item: "Date1" },
                    //     { date: new Date().toLocaleDateString("en-US"), item: "Date2" },
                    //     { date: new Date().toLocaleDateString("en-US"), item: "Date3" }
                    // ]}
                    items = {appointmentsList}/>

        </div>
    );
}
