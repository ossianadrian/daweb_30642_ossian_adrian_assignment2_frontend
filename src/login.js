import {
    Row,
    Container,
    Col,
    Carousel,
    ListGroup,
    Card,
    Button,
    Form
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { Component } from "react";
import axios from "axios";

class Login extends Component {
    constructor(props) {
        super(props);
        this.validateForm = this.validateForm.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeInputPassword = this.handleChangeInputPassword.bind(this);
        this.handleChangeInputUsername = this.handleChangeInputUsername.bind(this);
        this.state = {
            email: "",
            password: ""
        };
    }

    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0;
    }

    handleSubmit(event) {
        event.preventDefault();

        axios.defaults.withCredentials = true;
        axios.get('http://127.0.0.1:8000/sanctum/csrf-cookie').then(response => {
            // console.log(response);
            axios.post('http://127.0.0.1:8000/login', this.state).then(res=> {
                console.log("Login successfully");
                localStorage.setItem('email',this.state.email);
                localStorage.setItem('password', this.state.password);
                localStorage.setItem('services', "Service1, Service2, Service3");
                localStorage.setItem('gdpr', "False");
                document.getElementById("theNavBar").style.visibility = "visible";
                window.location.href= '/PersonHome';

                console.log(res);
            })
        });

    }

    handleChangeInputPassword(event) {
        this.setState({ password: event.target.value });
    }

    handleChangeInputUsername(event) {
        this.setState({ email: event.target.value });
    }

    componentDidMount() {
        document.getElementById("theNavBar").style.visibility = "hidden";
    }

    render() {

        return (
            <div
                style={{
                    position: "absolute",
                    top: "50%",
                    left: "50%",
                    transform: "translate(-50%, -50%)"
                }}
            >
                <Form>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Username</Form.Label>
                        <Form.Control
                            placeholder="Enter email"
                            value={this.state.email}
                            onChange={this.handleChangeInputUsername}
                        />
                        <Form.Text className="text-muted">
                            We'll never share your information with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            placeholder="Password"
                            value={this.state.password}
                            onChange={this.handleChangeInputPassword}
                        />
                    </Form.Group>

                    <Button
                        variant="primary"
                        type="submit"
                        disabled={!this.validateForm()}
                        onClick={this.handleSubmit}
                    >
                        Submit
                    </Button>
                </Form>
            </div>
        );
    }
}

export default Login;