import {Nav} from "react-bootstrap"
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import Home from "./Home.js"
import News from "./News.js"
import AboutUs from "./AboutUs.js"
import Doctors from "./Doctors.js"
import ServicesAndPricing from "./ServicesAndPricing.js"
import ContactUs from "./ContactUs.js"
import PersonHome from "./person-home";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css"
import React, {Component, Suspense} from "react";
import i18next from 'i18next';
import {useTranslation, withTranslation} from 'react-i18next';
import Appointments from "./appointments";
import Login from "./login"
import DoctorHome from "./Doctor-home"


class App extends Component {


  constructor(props) {
    super(props);

    this.changeLanguage = this.changeLanguage.bind(this);
    this.logOut = this.logOut.bind(this);


    this.state = {
      sender: ""
    };

  }

  logOut(){
    localStorage.removeItem('email');
    localStorage.removeItem('password');
    localStorage.removeItem('services');
    localStorage.removeItem('gdpr');
    window.location.href= '/Login';
  }


  changeLanguage(lang) {

    i18next.changeLanguage(lang);
  }


  render() {
    const { t } = this.props;

    return (


        <div className="App">
          <header className="nav-bar" >
            <Router>
              <Nav id="theNavBar" className="navbar navbar-expand-lg navbar-light bg-light" >
                <div className="container-fluid">
                  <a className="navbar-brand" href="https://ro.wikipedia.org/wiki/Doctor">
                    <img src="img/icon-medical.png" alt="" width="90" className="d-inline-block align-center"/>
                  </a>
                  <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                          aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                  </button>
                  <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                      <li className="nav-item">
                        <Link className="nav-link active" aria-current="page" to="/">{t('Navbar.Home')}</Link>
                      </li>
                      <li className="nav-item">
                        <Link className="nav-link" aria-current="page" to="/News">{t('Navbar.News')}</Link>
                      </li>
                      <li className="nav-item">
                        <Link className="nav-link" to="/AboutUs">{t('Navbar.AboutUs')}</Link>
                      </li>
                      <li className="nav-item">
                        <Link className="nav-link" to="/Doctors">{t('Navbar.Doctors')}</Link>
                      </li>
                      <li className="nav-item">
                        <Link className="nav-link" to="/ServicesAndPricing">{t('Navbar.ServicesAndPricing')}</Link>
                      </li>
                      <li className="nav-item">
                        <Link className="nav-link" to="/ContactUs">{t('Navbar.ContactUs')}</Link>
                      </li>
                      <li className="nav-item">
                        <Link className="nav-link" to="/PersonHome">Person Home</Link>
                      </li>
                      <li className="nav-item">
                        <Link className="nav-link" to="/Appointments">Appointments</Link>
                      </li>
                      {/*<li className="nav-item" >*/}
                      {/*  <Link className="nav-link" to="/Login">Login</Link>*/}
                      {/*</li>*/}
                      {/*<li className="nav-item" >*/}
                      {/*  <Link className="nav-link" to="/DoctorHome">Doctor home</Link>*/}
                      {/*</li>*/}
                      <li >
                        <button  type="button" className="btn btn-primary" onClick={() => this.logOut()}>Log Out
                        </button>
                      </li>
                      <li >
                        <button  type="button" className="btn btn-secondary" onClick={() => this.changeLanguage('en')}>EN
                        </button>
                      </li>
                      <li >
                        <button  type="button" className="btn btn-dark" onClick={() => this.changeLanguage('ro')}>RO
                        </button>
                      </li>

                    </ul>
                  </div>
                </div>
              </Nav>
              <Switch id="theSwitch">
                <Route exact path="/">
                  <Home/>
                </Route>
                <Route exact path="/News">
                  <News/>
                </Route>
                <Route exact path="/AboutUs">
                  <AboutUs/>
                </Route>
                <Route exact path="/Doctors">
                  <Doctors/>
                </Route>
                <Route exact path="/ServicesAndPricing">
                  <ServicesAndPricing/>
                </Route>
                <Route exact path="/ContactUs">
                  <ContactUs/>
                </Route>
                <Route exact path="/PersonHome">
                  <PersonHome/>
                </Route>
                <Route exact path="/Appointments">
                  <Appointments/>
                </Route>
                <Route exact path="/Login" >
                  <Login/>
                </Route>
                <Route exact path="/DoctorHome" >
                  <DoctorHome/>
                </Route>
              </Switch>
            </Router>
          </header>


          <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js"
                  integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi"
                  crossOrigin="anonymous"></script>
          <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js"
                  integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG"
                  crossOrigin="anonymous"></script>

        </div>

    );
  }
}

export default withTranslation()(App);
